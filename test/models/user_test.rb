# == Schema Information
#
# Table name: users
#
#  id                           :bigint           not null, primary key
#  account_holder_name          :string
#  avatar_url                   :text
#  bank_account_number          :string
#  bank_name                    :string
#  business_kind                :integer          default("private_business"), not null
#  company_name                 :string
#  company_registration_doc_url :text
#  company_registration_number  :string
#  email                        :string           not null
#  name                         :string           not null
#  office_address               :text
#  password_digest              :string
#  phone_number                 :string
#  representative_name          :string
#  username                     :string
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  notification_device_id       :text
#
require "test_helper"

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
