# == Schema Information
#
# Table name: auctions
#
#  id             :bigint           not null, primary key
#  bid_kind       :integer          not null
#  description    :text
#  end_time       :datetime         not null
#  kind           :integer          not null
#  preview_urls   :json
#  start_time     :datetime         not null
#  status         :integer          default("active"), not null
#  total          :decimal(22, 2)   default(0.0), not null
#  unit_price     :decimal(22, 2)   default(0.0), not null
#  volume         :decimal(10, 2)   default(0.0), not null
#  winner_tx_hash :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint
#  variant_id     :bigint           not null
#  winning_bid_id :bigint
#
# Indexes
#
#  index_auctions_on_user_id         (user_id)
#  index_auctions_on_variant_id      (variant_id)
#  index_auctions_on_winning_bid_id  (winning_bid_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#  fk_rails_...  (variant_id => variants.id)
#  fk_rails_...  (winning_bid_id => bids.id)
#
require "test_helper"

class AuctionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
