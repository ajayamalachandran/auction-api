#!/bin/bash
echo "Running migrations"
RAILS_ENV=$RAILS_ENV rake db:migrate
echo "Start server"
bundle exec rails s -u puma -e $RAILS_ENV
