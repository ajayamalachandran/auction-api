require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
ENV["RAILS_ADMIN_THEME"] = "softwarebrothers_theme"

Bundler.require(*Rails.groups)

module EMetalExchangeApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1
    config.i18n.locale = :ko
    config.i18n.fallbacks = [:en]
    config.assets.precompile += %w(rails_admin/rails_admin.css rails_admin/rails_admin.js)

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
