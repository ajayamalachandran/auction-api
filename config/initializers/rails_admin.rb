require "nested_form/engine"
require "nested_form/builder_mixin"

RailsAdmin.config do |config|
  config.parent_controller = "::ApplicationController"

  config.main_app_name = ["E-Metal Exchange", "관리 콘솔"]

  config.authorize_with do
    authenticate_or_request_with_http_basic(
      "Login required"
    ) do |name, password|
      (name == Rails.application.credentials.rails_admin[:username] &&
       password == Rails.application.credentials.rails_admin[:password])
    end
  end

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model "User" do
    weight -200
    include_all_fields
    edit do 
      field :password, :string
    end
    field :created_at do
      sort_reverse true
    end

    configure :company_registration_doc_url do 
      pretty_value do
         bindings[:view].content_tag(:a, "View Company Registration Doc", target: "_blank", href: bindings[:object].company_registration_doc_url) if bindings[:object].company_registration_doc_url.present?
      end
    end


    exclude_fields :password_digest
  end

  config.model "Category" do
    weight -100
  end

  config.model "Variant" do
    weight -90
    object_label_method do
      :name_with_category
    end
  end

  config.model "Auction" do
    weight -80
    object_label_method do
      :name
    end
    include_all_fields

    show do
      field :tx_link do
        formatted_value do
          %{<a target="_blank" href="#{bindings[:object].winning_tx_link}">View on Blockchain</a>}.html_safe if bindings[:object].winning_tx_link.present?
        end
      end

    end
  end

  config.model "Bid" do
    weight -70
  end

  config.model "Notification" do
    weight -10
  end
end
