Rails.application.routes.draw do
  mount RailsAdmin::Engine => "/admin", as: "rails_admin"
  mount Sidekiq::Web => "/sidekiq" # monitoring console
  get "/ping" => "application#ping"

  resources :users
  put "login" => "users#login"
  put "me" => "users#update"
  get "me" => "users#show"

  resources :auctions
  get "variants" => "auctions#variants"
  get "categories" => "auctions#categories"
  get "auction" => "auctions#show"
  get "bids" => "auctions#bids"
  get "tickers" => "auctions#tickers"
  post "place-bid" => "auctions#place_bid"
  get "notifications" => "users#notifications"
  post "signed-url" => "users#generate_signed_url"
  root to: redirect("/admin")

end
