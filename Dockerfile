FROM 998414664654.dkr.ecr.ap-northeast-2.amazonaws.com/ruby:2.7.2

RUN apt-get update -qq && apt-get install -y build-essential nodejs 

ENV RAILS_ROOT /home/ubuntu/e-metal-api

RUN mkdir -p $RAILS_ROOT/tmp/pids

WORKDIR $RAILS_ROOT

COPY Gemfile Gemfile

COPY Gemfile.lock Gemfile.lock

RUN gem install bundler

RUN bundle install --without development test

COPY config/puma.rb config/puma.rb

RUN RAILS_ENV=production rails assets:precompile

COPY . .

EXPOSE 3000


