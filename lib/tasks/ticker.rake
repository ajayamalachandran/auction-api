namespace :ticker do
  desc "1 minute ticker"
  task complete_auctions: :environment do
    Auction.complete_auctions!
  end
end
