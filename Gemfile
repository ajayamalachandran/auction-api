source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "2.7.2"

gem "active_type"
gem "autoprefixer-rails"
gem "bcrypt", "~> 3.1.7"
gem "bootsnap", ">= 1.4.4", require: false
gem "coffee-rails"
gem "pg", "~> 1.1"
gem "pgcli-rails"
gem "puma", "~> 5.0"
gem "rack-canonical-host"
gem "rails", "~> 6.1.2"
gem "redis", "~> 4.0"
gem "sass-rails"
gem "aws-sdk-s3", require: true
gem "will_paginate", "~> 3.1.0"
gem "rest-client"
gem "jwt"
gem "sidekiq"
gem "turbolinks", "~> 5"
gem "webpacker"
gem "jbuilder"
gem "rails_admin"
gem "rails_admin_softwarebrothers_theme", :git => "https://github.com/5hanth/rails_admin_softwarebrothers_theme.git"
gem "rack-cors"
gem "arask"
gem 'fcm', '~> 0.0.1'
# gem 'rails-i18n'

group :production do
  gem "postmark-rails"
end

group :development do
  gem "pry"
  gem "amazing_print"
  gem "solargraph"
  gem "annotate"
  gem "guard", require: false
  gem "guard-minitest", require: false
  gem "letter_opener"
  gem "listen", "~> 3.3"
  gem "spring"
  gem "terminal-notifier", require: false
  gem "terminal-notifier-guard", require: false
  gem "web-console", ">= 4.1.0"
end

group :development, :test do
  gem "brakeman", require: false
  gem "bundler-audit", require: false
  gem "byebug"
  gem "dotenv-rails"
  gem "launchy"
  gem "rubocop", require: false
  gem "rubocop-minitest", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
end

group :test do
  gem "capybara", ">= 3.26"
  gem "minitest-ci", require: false
  gem "selenium-webdriver"
  gem "shoulda-context"
  gem "shoulda-matchers"
  gem "webdrivers"
end
