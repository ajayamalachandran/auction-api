# == Schema Information
#
# Table name: tickers
#
#  id              :bigint           not null, primary key
#  name            :string           not null
#  price           :string
#  price_xpath     :text
#  trillions_price :string
#  url             :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require "open-uri"

class Ticker < ApplicationRecord
  def self.sync_all
    Ticker.all.each do |tick|
      tick.update_price_from_xpath!
    end
  end

  def update_price_from_xpath!
    return unless url.present? and price_xpath.present?
    begin
      doc = Nokogiri::HTML(URI.open(url))
      new_price = doc.xpath(price_xpath).text
      Rails.logger.debug "Got new price #{new_price} for #{name}"
    rescue => e
      Rails.logger.debug e.message
      new_price = trillions_price
    end
    new_price = "$#{new_price}" if self.name.match 'LME'
    update!(price: new_price) if new_price.present?
  end
end
