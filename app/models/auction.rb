# == Schema Information
#
# Table name: auctions
#
#  id             :bigint           not null, primary key
#  bid_kind       :integer          not null
#  description    :text
#  end_time       :datetime         not null
#  kind           :integer          not null
#  preview_urls   :json
#  start_time     :datetime         not null
#  status         :integer          default("active"), not null
#  total          :decimal(22, 2)   default(0.0), not null
#  unit_price     :decimal(22, 2)   default(0.0), not null
#  volume         :decimal(10, 2)   default(0.0), not null
#  winner_tx_hash :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint
#  variant_id     :bigint           not null
#  winning_bid_id :bigint
#
# Indexes
#
#  index_auctions_on_user_id         (user_id)
#  index_auctions_on_variant_id      (variant_id)
#  index_auctions_on_winning_bid_id  (winning_bid_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#  fk_rails_...  (variant_id => variants.id)
#  fk_rails_...  (winning_bid_id => bids.id)
#
class Auction < ApplicationRecord
  belongs_to :variant
  belongs_to :user

  has_many :notifications, as: :entity, dependent: :destroy

  has_many :bids, dependent: :destroy

  belongs_to :winning_bid, class_name: "Bid", foreign_key: "winning_bid_id", optional: true

  enum kind: %i[normal_auction reverse_auction]
  enum bid_kind: %i[buy sell]
  enum status: %i[active complete]

  before_save :set_total

  after_update :create_notifications_for_completed_auction!, if: :saved_change_to_status?
  default_scope -> { order("created_at DESC") }
  scope :search, ->(query) {
          query = "%#{query.downcase.strip}%"
          joins(:variant).joins(:variant => :category).where("lower(variants.name) like ? or lower(categories.name) like ? or lower(description) like ?", query, query, query)
        }
  class << self
    def complete_auctions!
      auctions = Auction.active.where("end_time < ?", Time.now)
      Rails.logger.debug "Total completed auctions #{auctions.count}"
      # auctions.each do |auction|
      #   auction.set_winner!
      # end
    end
  end

  def name
    return id unless bid_kind.present?
    "#{id} - #{variant.name_with_category}"
  end

  def my_bid
    return unless Current.user.present?
    Current.user.bids.where(auction_id: id).last
  end

  def bidders
    bids.order("id DESC")
  end

  def set_winner!
    Rails.logger.debug "Setting winning bid #{best_bid.id if best_bid.present?}"
    if best_bid.present?
      tx_hash = CoinManager.recordAuctionWinner auction_id: id, winning_bid_id: best_bid.id
    end
    update!(winning_bid: best_bid, status: :complete, winner_tx_hash: tx_hash)
  end

  def winning_tx_link
    ""
    # "#{Rails.application.credentials.dig(:coinmanager, :chain_url)}/tx/#{winner_tx_hash}" if winner_tx_hash.present?
  end

  def best_bid
    if normal_auction?
      if buy?
        winner = bids.order("bid_amount ASC").first
      else
        winner = bids.order("bid_amount DESC").first
      end
    else
      if buy?
        winner = bids.order("bid_amount DESC").first
      else
        winner = bids.order("bid_amount ASC").first
      end
    end
    winner
  end

  private

  def create_notifications_for_completed_auction!
    return unless complete?
    if winning_bid.present?
      winning_user = winning_bid.user
      winning_user.notifications.bid_won.create! entity: winning_bid
      lost_user_ids = bids.where.not(user_id: winning_user.id).pluck(:user_id)
      User.where(id: lost_user_ids).each do |lost_user|
        lost_user.notifications.bid_lost.create! entity: winning_bid
      end
      user.notifications.auction_sold.create! entity: winning_bid
    else
      user.notifications.auction_unsold.create! entity: self
    end
  end

  def set_total
    self.total = self.unit_price * self.volume
  end
end
