# == Schema Information
#
# Table name: notifications
#
#  id          :bigint           not null, primary key
#  entity_type :string           not null
#  kind        :integer          not null
#  message     :text
#  read_at     :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  entity_id   :bigint           not null
#  user_id     :bigint           not null
#
# Indexes
#
#  index_notifications_on_entity   (entity_type,entity_id)
#  index_notifications_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Notification < ApplicationRecord
  belongs_to :user
  belongs_to :entity, polymorphic: true

  enum kind: %i[bid_received auction_sold auction_unsold bid_won bid_lost admin_notice]

  before_create :set_message
  default_scope -> { order("created_at DESC") }

  private

  def set_message
    case kind.to_sym
    when :bid_received
      bid = entity
      auction = bid.auction
      bidding_user = bid.user
      self.message = "You have received a bid from #{bidding_user.name} for your auction #{auction.name} at price ₩#{bid.bid_amount}"
      FirebaseMessenger.fcm_push_notification(self.message,auction.user.notification_device_id)

    when :auction_sold
      auction = entity
      winning_bid = auction.winning_bid
      bidding_user = winning_bid.user
      self.message = "Your auction #{auction.name} is successfully completed at price ₩#{winning_bid.bid_amount} by #{bidding_user.name}"
      FirebaseMessenger.fcm_push_notification(self.message,auction.user.notification_device_id)

    when :auction_unsold
      auction = entity
      self.message = "Your auction #{auction.name} has ended with no successful bidders"
      FirebaseMessenger.fcm_push_notification(self.message,auction.user.notification_device_id)

    when :bid_won
      bid = entity
      auction = bid.auction
      bidding_user = bid.user
      self.message = "Your bid for auction #{auction.name} is successfully won at price ₩#{bid.bid_amount}"
      FirebaseMessenger.fcm_push_notification(self.message,bidding_user.notification_device_id)

    when :bid_lost
      bid = entity
      auction = bid.auction
      bidding_user = bid.user
      winning_bid = auction.winning_bid
      self.message = "Your bid for auction #{auction.name} is not successful as your bid price ₩#{bid.bid_amount} is not the best price #{winning_bid.bid_amount} by #{winning_bid.user.name}"
      FirebaseMessenger.fcm_push_notification(self.message,bidding_user.notification_device_id)
    end
  end
end
