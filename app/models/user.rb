# == Schema Information
#
# Table name: users
#
#  id                           :bigint           not null, primary key
#  account_holder_name          :string
#  avatar_url                   :text
#  bank_account_number          :string
#  bank_name                    :string
#  business_kind                :integer          default("private_business"), not null
#  company_name                 :string
#  company_registration_doc_url :text
#  company_registration_number  :string
#  email                        :string           not null
#  name                         :string           not null
#  office_address               :text
#  password_digest              :string
#  phone_number                 :string
#  representative_name          :string
#  username                     :string
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  notification_device_id       :text
#
class User < ApplicationRecord
  has_secure_password
  before_validation :generate_username_if_not_present

  validates :email, presence: true, uniqueness: true
  validates :username, presence: true, uniqueness: true

  has_many :notifications, dependent: :destroy
  has_many :auctions, dependent: :destroy
  has_many :bids, dependent: :destroy

  has_many :participated_auctions, through: :bids, source: :auction, dependent: :destroy

  enum business_kind: %i[private_business corporate_business]

  def token
    Auth.create_token({ id: id, email: email })
  end

  private

  def generate_username_if_not_present
    return if self.username.present?
    self.username = "#{name}#{company_name}".gsub(/\s+/, "") if (name || company_name).present?
    self.username = self.email[/^[^@]+/].gsub(/\s+/, "") unless self.username.present?

    if User.where(username: self.username).exists?
      self.username = "#{self.username}#{User.last.id}#{SecureRandom.hex[0..5]}".gsub(/\s+/, "")
    end
  end
end
