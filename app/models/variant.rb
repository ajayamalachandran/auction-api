# == Schema Information
#
# Table name: variants
#
#  id          :bigint           not null, primary key
#  name        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint           not null
#
# Indexes
#
#  index_variants_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#
class Variant < ApplicationRecord
  belongs_to :category
  default_scope -> { order("created_at DESC") }

  def name_with_category
    return name unless category.present?
    "#{category.name} - #{name}"
  end
end
