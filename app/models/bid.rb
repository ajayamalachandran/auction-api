# == Schema Information
#
# Table name: bids
#
#  id         :bigint           not null, primary key
#  bid_amount :decimal(22, 2)   default(0.0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  auction_id :bigint           not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_bids_on_auction_id  (auction_id)
#  index_bids_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (auction_id => auctions.id)
#  fk_rails_...  (user_id => users.id)
#
class Bid < ApplicationRecord
  belongs_to :user
  belongs_to :auction
  has_many :notifications, as: :entity, dependent: :destroy
  default_scope -> { order("created_at DESC") }

  after_commit :create_notifications_for_placed_bid!, on: :create

  before_validation :ensure_user_can_bid

  private

  def create_notifications_for_placed_bid!
    auction_owner = auction.user
    auction_owner.notifications.bid_received.create! entity: self
  end

  def ensure_user_can_bid
    # raise "이 경매는 이미 종료되었습니다" if auction.end_time > Time.now
    raise "자신의 경매에 입찰 할 수 없습니다." if user == auction.user
    raise "이 경매에 대해 이미 입찰했습니다." if auction.bids.where(user_id: user_id).exists?
  end
end
