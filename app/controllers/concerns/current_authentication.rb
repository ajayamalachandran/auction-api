module CurrentAuthentication
  extend ActiveSupport::Concern

  included do
    before_action :authenticate

    def current_user
      id = Auth.decode_token(request.headers["Authorization"])["claims"]["id"] rescue nil
      User.find(id) if id.present?
    end
  end

  private

  def authenticate
    Current.user = current_user
  end
end
