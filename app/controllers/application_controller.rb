class ApplicationController < ActionController::Base
  include BasicAuth
  protect_from_forgery with: :null_session
  rescue_from Exception, with: :render_error
  include CurrentAuthentication
  include SetCurrentRequestDetails
  before_action :set_locale

  def ping
    render json: { pong: true }
  end

  def requires_auth!
    raise "You cannot perform this operation" unless Current.user.present?
  end

  private

  def render_error(e)
    render json: { error: true, message: e.message }
  end

  def set_locale
    I18n.locale = :ko
  end
end
