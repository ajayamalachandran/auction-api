class AuctionsController < ApplicationController
  before_action :requires_auth!

  def create
    @auction = Current.user.auctions.create!(auction_params)
  end

  def update
    @auction = Current.user.auctions.where(id: auction_params[:id]).first
    @auction.update!(auction_params)
    render json: { message: "Updated auction successfully" }
  end

  def show
    @auction = Auction.where(id: params[:id]).first
    raise "Auction not found" unless @auction.present?
  end

  def destroy
    @auction = Current.user.auctions.where(id: auction_params[:id]).first
    @auction.destroy!
    render json: { message: "Deleted auction successfully" }
  end

  def index
    if params[:filter] == "mine"
      @auctions = Current.user.auctions
    elsif params[:filter] == "participated"
      @auctions = Current.user.participated_auctions
    else
      @auctions = Auction.all
    end

    if params[:kind] == "normal_auction"
      @auctions = @auctions.normal_auction
    elsif params[:kind] == "reverse_auction"
      @auctions = @auctions.reverse_auction
    end

    if params[:bid_kind] == "buy"
      @auctions = @auctions.buy
    elsif params[:bid_kind] == "sell"
      @auctions = @auctions.sell
    end

    @auctions = @auctions.search(params[:query]) if params[:query].present?
    @auctions = @auctions.paginate(page: params[:page] || 1, per_page: 24)
  end

  def variants
    @variants = Variant.all
  end

  def categories
    @categories = Category.all
  end

  def place_bid
    @bid = Current.user.bids.create!(bid_params)
  end

  def bids
    @bids = Bid.where(auction_id: params[:auction_id])
    @bids = @bids.paginate(page: params[:page] || 1, per_page: 24)

  end

  def tickers
    @tickers = Ticker.all
  end

  private

  def bid_params
    params.require(:bid).permit(:auction_id, :bid_amount)
  end

  def auction_params
    params.require(:auction).permit(:id,
                                    :kind,
                                    :unit_price,
                                    :volume,
                                    :total,
                                    :start_time,
                                    :end_time,
                                    :bid_kind,
                                    :variant_id,
                                    :description, preview_urls: [])
  end
end
