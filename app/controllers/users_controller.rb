class UsersController < ApplicationController
  before_action :requires_auth!, only: [:show, :update, :notifications, :generate_signed_url]

  def create
    @user = User.find_by(email: user_params[:email])
    raise "You're are already signed up, please sign in" if @user.present?
    @user = User.create!(user_params)
  end

  def login
    @user = User.find_by(email: user_params[:email])
    raise "Invaild credentials" unless @user.present? and @user.authenticate(user_params[:password])
  end

  def update
    Current.user.update!(user_params)
    render json: { message: "Updated profile successfully" }
  end

  def show
    @user = Current.user
  end

  def notifications
    @notifications = Current.user.notifications
  end

  def generate_signed_url
    url = S3.signed_url(Current.user.id, params[:key])
    render json: { url: url }
  end

  private

  def user_params
    params.require(:user).permit(:id, :name, :email, :bank_name, :bank_account_number, :business_kind, :avatar_url,:notification_device_id,
                                 :username,
                                 :phone_number,
                                 :account_holder_name,
                                 :company_name,
                                 :representative_name,
                                 :company_registration_number,
                                 :password, :company_registration_doc_url, :office_address)
  end
end
