require "aws-sdk-s3"

class S3
  def self.signed_url(user_id, key)
    @signer ||= Aws::S3::Presigner.new(client: Aws::S3::Client.new)
    url =
      @signer.presigned_url(
        :put_object,
        bucket: Rails.application.credentials.dig(:aws, :s3_bucket),
        key: "#{Rails.env.to_sym}/#{user_id}/#{Time.now.to_i}-#{SecureRandom.uuid}/#{key}",
      )
    url
  end
end
