class CoinManager
  class << self
    def base_uri
      @base_uri ||= Rails.application.credentials.dig(:coinmanager, :endpoint)
    end

    def headers
      @headers ||= { 'coin-manager-api-key': Rails.application.credentials.dig(:coinmanager, :api_key) }
    end

    def recordAuctionWinner(params)
      resp = RestClient.get(base_uri + "/emetal-record-data?auctionId=#{params[:auction_id]}&winnerBidId=#{params[:winning_bid_id]}", headers)
      parsed = JSON.parse(resp)
      parsed["txHash"]
    end
  end
end
