class Auth
  def self.create_token(data)
    payload = {
      :exp => (Time.now + 3.months).to_i,
      :uid => data[:id],
      :claims => data,
    }
    JWT.encode payload, private_key, algorithm
  end

  def self.decode_token(jwt_token)
    JWT.decode(jwt_token, private_key, true, { algorithm: algorithm })[0].with_indifferent_access
  end

  # def self.verify_google_id_token(token)
  #   validator.check(token, google_client_id)
  # end

  private

  def self.private_key
    @key = Rails.application.credentials.jwt[:secret_key]
  end

  def self.algorithm
    @key = Rails.application.credentials.jwt[:algorithm]
  end

  def self.validator
    @checker ||= GoogleIDToken::Validator.new
  end

  def self.google_client_id
    @client ||= Rails.application.credentials.dig(:google, Current.platform, :client_id)
  end
end
