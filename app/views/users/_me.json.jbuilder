json.(user, :id, :name, :email, :bank_name, :bank_account_number, :business_kind, :avatar_url,
      :username,
      :phone_number,
      :account_holder_name,
      :company_name,
      :representative_name,
      :company_registration_number, :company_registration_doc_url, :office_address)
