json.(user, :id, :name, :email, 
      :username)

json.avatar_url user.avatar_url if user.avatar_url.present?
