json.partial! "users/me", user: @user
json.token @user.token

json.message "Signed up successfully"
