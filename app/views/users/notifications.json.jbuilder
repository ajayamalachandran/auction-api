json.array! @notifications do |notification|
  json.user do
    json.partial! "users/user", user: notification.user
  end
  json.(notification, :message, :read_at, :kind)
  if notification.entity.is_a? Auction
    json.auction do
      json.partial! "auctions/auction", auction: notification.entity
    end
  elsif notification.entity.is_a? Bid
    json.bid do
      json.partial! "auctions/bid", bid: notification.entity
    end
    json.auction do
      json.partial! "auctions/auction", auction: notification.entity.auction
    end
  end
end
