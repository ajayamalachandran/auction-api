json.array! @tickers do |ticker|
  json.(ticker, :name, :price, :trillions_price)
end
