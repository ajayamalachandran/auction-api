json.(auction, :name, :id,
      :kind,
      :unit_price,
      :volume,
      :total,
      :start_time,
      :end_time,
      :bid_kind,
      :variant_id,
      :preview_urls, :description, :status, :winner_tx_hash, :winning_tx_link)

json.variant do
  json.partial! "auctions/variant", variant: auction.variant
end

json.bids do
  json.array! auction.bids, partial: "auctions/bid", as: :bid
end

json.my_bid do
  json.partial! "auctions/bid", bid: auction.my_bid if auction.my_bid.present?
end

json.best_bid do
  json.partial! "auctions/bid", bid: auction.best_bid if auction.best_bid.present?
end

json.winning_bid do
  json.partial! "auctions/bid", bid: auction.winning_bid if auction.winning_bid.present?
end

json.bids_count auction.bids.count
