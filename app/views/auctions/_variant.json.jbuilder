json.(variant, :name, :id, :name_with_category)

json.category do
  json.partial! "auctions/category", category: variant.category
end
