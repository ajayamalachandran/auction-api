json.(bid, :bid_amount, :auction_id, :id, :created_at, :updated_at)
json.user do
  json.partial! "users/user", user: bid.user
end
