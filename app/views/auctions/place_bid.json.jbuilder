json.my_bid do
  json.partial! "auctions/bid", bid: @bid
end

json.message "Placed bid successfully"
