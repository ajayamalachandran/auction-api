# Ticker.create!(name: '알루미늄(서구산)',  price_xpath: "//*[contains(text(), '알루미늄(서구산)')]/ancestor::tr/td[2]", url: "https://www.pps.go.kr/bichuk/index.do")
# Ticker.create!(name: "LME ALUMINIUM", price_xpath: "//*[contains(text(), 'Cash')]/ancestor::tr/td[2]", url: "https://www.lme.com/en-GB/Metals/Non-ferrous/Aluminium#tabIndex=0")

users = File.read Rails.root + "db/users.csv"
parsed_users = CSV.parse users

parsed_users.drop(1).each do |company_name, company_registration_number, office_address, phone_number, _, name|
  next unless name.present?
  @user = User.create!(company_name: company_name, company_registration_number: company_registration_number, office_address: office_address, phone_number: phone_number, name: name, account_holder_name: name, representative_name: name, email: "#{SecureRandom.hex}@trillionslab.com", password: company_registration_number || phone_number || name)
  puts @user.id
end
