class CreateAuctions < ActiveRecord::Migration[6.1]
  def change
    create_table :auctions do |t|
      t.integer :kind, null: false, limit: 3
      t.decimal :unit_price, precision: 22, scale: 2, default: "0.0", null: false
      t.decimal :volume, precision: 10, scale: 2, default: "0.0", null: false
      t.decimal :total, precision: 22, scale: 2, default: "0.0", null: false
      t.timestamp :start_time, null: false
      t.timestamp :end_time, null: false
      t.integer :bid_kind, null: false, limit: 3
      t.references :variant, null: false, foreign_key: true
      t.json :preview_urls

      t.timestamps
    end
  end
end
