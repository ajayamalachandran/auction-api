class AddOfficeAddressToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :office_address, :text
  end
end
