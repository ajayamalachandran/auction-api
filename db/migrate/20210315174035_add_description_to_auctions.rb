class AddDescriptionToAuctions < ActiveRecord::Migration[6.1]
  def change
    add_column :auctions, :description, :text
    add_column :auctions, :status, :integer, limit: 4, default: 0, null: false
  end
end
