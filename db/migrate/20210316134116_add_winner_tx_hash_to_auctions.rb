class AddWinnerTxHashToAuctions < ActiveRecord::Migration[6.1]
  def change
    add_column(:auctions, :winner_tx_hash, :text)
  end
end
