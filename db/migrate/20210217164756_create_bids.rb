class CreateBids < ActiveRecord::Migration[6.1]
  def change
    create_table :bids do |t|
      t.references :user, null: false, foreign_key: true
      t.references :auction, null: false, foreign_key: true
      t.decimal :bid_amount, precision: 22, scale: 2, default: "0.0", null: false

      t.timestamps
    end
  end
end
