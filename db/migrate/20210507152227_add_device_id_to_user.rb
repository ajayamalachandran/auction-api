class AddDeviceIdToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :notification_device_id, :text 
  end
end
