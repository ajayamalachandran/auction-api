class CreateTickers < ActiveRecord::Migration[6.1]
  def change
    create_table :tickers do |t|
      t.string :name, null: false
      t.text :url
      t.text :price_xpath
      t.string :price
      t.string :trillions_price

      t.timestamps
    end
  end
end
