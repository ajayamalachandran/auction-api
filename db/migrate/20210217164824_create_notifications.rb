class CreateNotifications < ActiveRecord::Migration[6.1]
  def change
    create_table :notifications do |t|
      t.integer :kind, null: false, limit: 4
      t.references :user, null: false, foreign_key: true
      t.text :message
      t.timestamp :read_at
      t.references :entity, polymorphic: true, null: false

      t.timestamps
    end
  end
end
