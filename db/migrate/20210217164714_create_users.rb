class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :bank_name
      t.string :bank_account_number
      t.integer :business_kind, null: false, default: 0, limit: 3

      t.timestamps
    end
  end
end
