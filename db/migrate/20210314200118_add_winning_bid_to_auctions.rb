class AddWinningBidToAuctions < ActiveRecord::Migration[6.1]
  def change
    add_reference :auctions, :winning_bid, foreign_key: { to_table: :bids }
  end
end
