class AddFieldsToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :username, :string, index: true, unique: true
    add_column :users, :phone_number, :string
    add_column :users, :account_holder_name, :string
    add_column :users, :company_name, :string
    add_column :users, :representative_name, :string
    add_column :users, :company_registration_number, :string
  end
end
