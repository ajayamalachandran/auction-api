# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_07_152227) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "arask_jobs", force: :cascade do |t|
    t.string "job"
    t.datetime "execute_at"
    t.string "interval"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["execute_at"], name: "index_arask_jobs_on_execute_at"
  end

  create_table "auctions", force: :cascade do |t|
    t.integer "kind", null: false
    t.decimal "unit_price", precision: 22, scale: 2, default: "0.0", null: false
    t.decimal "volume", precision: 10, scale: 2, default: "0.0", null: false
    t.decimal "total", precision: 22, scale: 2, default: "0.0", null: false
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
    t.integer "bid_kind", null: false
    t.bigint "variant_id", null: false
    t.json "preview_urls"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.bigint "winning_bid_id"
    t.text "description"
    t.integer "status", default: 0, null: false
    t.text "winner_tx_hash"
    t.index ["user_id"], name: "index_auctions_on_user_id"
    t.index ["variant_id"], name: "index_auctions_on_variant_id"
    t.index ["winning_bid_id"], name: "index_auctions_on_winning_bid_id"
  end

  create_table "bids", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "auction_id", null: false
    t.decimal "bid_amount", precision: 22, scale: 2, default: "0.0", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["auction_id"], name: "index_bids_on_auction_id"
    t.index ["user_id"], name: "index_bids_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "kind", null: false
    t.bigint "user_id", null: false
    t.text "message"
    t.datetime "read_at"
    t.string "entity_type", null: false
    t.bigint "entity_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["entity_type", "entity_id"], name: "index_notifications_on_entity"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "tickers", force: :cascade do |t|
    t.string "name", null: false
    t.text "url"
    t.text "price_xpath"
    t.string "price"
    t.string "trillions_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "bank_name"
    t.string "bank_account_number"
    t.integer "business_kind", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "password_digest"
    t.text "avatar_url"
    t.string "username"
    t.string "phone_number"
    t.string "account_holder_name"
    t.string "company_name"
    t.string "representative_name"
    t.string "company_registration_number"
    t.text "company_registration_doc_url"
    t.text "office_address"
    t.text "notification_device_id"
  end

  create_table "variants", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_variants_on_category_id"
  end

  add_foreign_key "auctions", "bids", column: "winning_bid_id"
  add_foreign_key "auctions", "users"
  add_foreign_key "auctions", "variants"
  add_foreign_key "bids", "auctions"
  add_foreign_key "bids", "users"
  add_foreign_key "notifications", "users"
  add_foreign_key "variants", "categories"
end
